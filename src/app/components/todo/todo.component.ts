import { Component, OnDestroy, OnInit } from '@angular/core';
import { TodoItemInterface } from 'src/app/interfaces/todo-item.interface';
import { TodoService } from 'src/app/services/todo.service';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent implements OnInit {

  todoName: string = '';
  // todoListData: string[] = [];
  todoListData: Array<string> = [];

  //różne możliwości otypowania zmiennej todoDB
  // todoDB: TodoItemInterface[] = [];
  todoDB: Array<TodoItemInterface> = [];

  constructor(private todoService: TodoService) { }

  ngOnInit(): void {
    // przypisanie do nowej tablicy, aby mieć dostęp z poziomu
    // komponentu
    this.todoListData = this.todoService.todoList;

    this.todoDB = this.todoService.todoListDB;
  }

  onBtnClick(newTodoName: string) {
    // console.log(newTodoName);
    this.todoService.onAddTodo(newTodoName);
  }
}
